extends MultiplayerSpawner


var client: Player


func get_players() -> Array[Player]:
	# Hacky method because get_children() returns an Array[Node] even though we
	# know that every child of the Players singleton should be a player.
	var players: Array[Player] = []
	players.append_array(get_children())
	
	return players


# Returns null if not found.
func get_sender() -> Variant:
	var sender_id := multiplayer.get_remote_sender_id()
	
	return get_from_peer_id(sender_id)


func get_player(id: int) -> Player:
	var players := get_players()
	
	for player in players:
		if player.user_id == id:
			return player
	
	return null


func get_from_peer_id(peer_id: int) -> Player:
	var players := get_players()
	
	for player in players:
		if player.peer_id == peer_id:
			return player
	
	return null


func get_from_username(username: String) -> Player:
	var players := get_players()
	
	for player in players:
		if player.username == username:
			return player
	
	return null


func insert(data: Array) -> void:
	# See login_manager.gd get_data for array values.
	var player := Player.new()
	
	player.user_id = data[0]
	player.username = data[1]
	_set_player_characters(player, data[5])
	
	add_child(player)


func remove(id: int) -> void:
	remove_child(get_player(id))


func _set_player_characters(player: Player, characters: String) -> void:
	# I honestly don't know whether it returns null or a blank string so this accounts for both.
	if not characters:
		return
	
	if characters.is_empty():
		return
	
	player.characters = Player.CharacterList.deserialize(characters)
