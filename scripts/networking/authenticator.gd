extends Node


const LoginManager := preload("res://scripts/networking/login_manager.gd")
var login_manager: LoginManager


func _ready() -> void:
	if Network.is_server:
		login_manager = LoginManager.new()
		login_manager.database = Database.new(self)


func authenticate(peer_id: int, bytes: PackedByteArray) -> void:
	if not Network.is_server:
		multiplayer.complete_auth(peer_id)
		return
	
	var request: Array = bytes_to_var(bytes)
	var username: String = request[0]
	var password: String = request[1]
	var is_register: bool = request[2]
	
	if not login_manager.is_request_valid(username, password):
		multiplayer.disconnect_peer(peer_id)
		return
	
	if is_register:
		_register(username, password, peer_id)
	else:
		_login(username, password, peer_id)


func _register(username: String, password: String, peer_id: int) -> void:
	if await login_manager.exists(username):
		multiplayer.disconnect_peer(peer_id)
		return
	
	var id := await login_manager.insert(username, password)
	var data: Array = await login_manager.get_data(id)
	
	Players.insert(data)
	multiplayer.complete_auth(peer_id)


func _login(username: String, password: String, peer_id: int) -> void:
	var data: Array = await login_manager.get_data_by_username(username)
	
	if not data:
		multiplayer.disconnect_peer(peer_id)
		return
	
	var id: int = data[0]
	var user_password: String = data[2]
	
	var hashed_password: String = await login_manager.hash_password(password, id)
	
	if user_password != hashed_password:
		multiplayer.disconnect_peer(peer_id)
	else:
		Players.insert(data)
		multiplayer.complete_auth(peer_id)
