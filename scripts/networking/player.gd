class_name Player
extends MultiplayerSynchronizer


const CharacterList := preload("res://scripts/character/character_list.gd")

var user_id: int
var peer_id: int
var username: String

var characters := CharacterList.new()


func _ready() -> void:
	name = username
	
	Network.peer_disconnected


func _exit_tree() -> void:
	characters.clear()
	
	if peer_id in multiplayer.get_peers():
		multiplayer.disconnect_peer(peer_id)


func _on_peer_disconnected(id: int) -> void:
	if id == peer_id:
		queue_free()
