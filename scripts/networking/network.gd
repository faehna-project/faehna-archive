extends Node


signal connected_to_server
signal connection_failed
signal peer_disconnected(peer_id: int)

var is_server := false

var _auth_request: Array

@onready var authenticator: Node = $Authenticator


func _init() -> void:
	if "--server" in OS.get_cmdline_user_args():
		is_server = true
	# TODO add custom port support


func _ready() -> void:
	multiplayer.server_relay = false
	multiplayer.connected_to_server.connect(_on_connected)
	
	if is_server:
		var peer := ENetMultiplayerPeer.new()
		
		peer.create_server(17007)
		
		multiplayer.multiplayer_peer = peer
		multiplayer.peer_disconnected.connect(_on_peer_disconnected)
		
		# Dedicated server should always run the world scene.
		if SceneTree.current_scene.name != "World":
			get_tree().change_scene_to_file("res://scenes/world/world_server.tscn")


func connect_to_server(address: String, port: int, auth_request: Array) -> void:
	var peer := ENetMultiplayerPeer.new()
	
	peer.create_client(address, port)
	multiplayer.multiplayer_peer = peer
	
	_auth_request = auth_request


func _on_connected() -> void:
	var expected_username: String = _auth_request[0]
	Players.client = Players.get_from_username(expected_username)
	
	connected_to_server.emit()


func _on_authenticating(peer_id: int) -> void:
	multiplayer.send_auth(peer_id, var_to_bytes(_auth_request))


func _on_peer_disconnected(peer_id: int) -> void:
	peer_disconnected.emit(peer_id)
