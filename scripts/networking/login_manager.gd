extends RefCounted


const MAX_NAME_LENGTH = 32
const PASSWORD_LENGTH = 64 # Passwords are always hashed to exactly 64 chars

var argon2 := Argon2.new()
var database: Database


func hash_password(password: String, user_id: int) -> String:
	return await argon2.hash_password(password, user_id)


func is_request_valid(username: String, password: String) -> bool:
	var name_too_long := username.length() > MAX_NAME_LENGTH
	var name_illegal := not username.is_valid_identifier()
	var password_length_invalid := password.length() != PASSWORD_LENGTH
	var password_not_hex := not password.is_valid_hex_number()
	
	if name_too_long or name_illegal or password_length_invalid or password_not_hex:
		return false
	else:
		return true


# Note: Returns null if does not exist.
func get_id(username: String) -> Variant:
	var result: Array = await database.get_values("players", "user_id",
			"username = '" + username + "'")
	
	if result.is_empty():
		return
	
	return result[0][0]


# Note: Returns null if does not exist.
# 0 - user_id
# 1 - username
# 2 - password
# 3 - created_on
# 4 - last_online
# 5 - characters (may be blank after a reset)
# 6 - patron_id
# 7 - rank (patreon tier or staff)
# 8 - notes (just an extra place to write stuff)
func get_data(id: int) -> Variant:
	var result: Array = await database.get_values("players", "*",
			"user_id = " + str(id))
	
	if result.is_empty():
		return
	
	return result[0]


func get_data_by_username(username: String) -> Variant:
	var result: Array = await database.get_values("players", "*",
			"username = '" + username + "'")
	
	if result.is_empty():
		return
	
	return result[0]


func exists(username: String) -> bool:
	var result := await database.get_values("players", "COUNT(*)",
			"LOWER(username) = LOWER('" + username + "')")
	
	if result[0][0] > 0:
		return true
	else:
		return false


func insert(username: String, password: String) -> int:
	var time := str(Time.get_unix_time_from_system())
	
	var table := "players (username, password, created_on, last_online)"
	var values := "('%s', '%s', %s, %s)"
	
	values = values % [username, "", time, time]
	
	await database.insert_values(table, values)
	
	var id: int = await get_id(username)
	await set_password(id, password)
	
	return id


func set_password(user_id: int, password: String) -> void:
	var hashed := await argon2.hash_password(password, user_id)
	
	var values := "password = '" + hashed + "'"
	var condition := "user_id = '" + str(user_id) + "'"
	
	await database.update_values("players", values, condition)


class Argon2:
	extends Node
	
	
	const COMMAND := 'echo -n "%s" | argon2 %s -t %d -m %d -r'
	const ITERATIONS := 16
	const MEMORY := 16
	
	var is_disabled: bool = "--no-argon2" in OS.get_cmdline_user_args()
	var is_standalone: bool = OS.has_feature("standalone")
	var os: String = OS.get_name()
	var program: String = _get_program()
	
	
	func hash_password(password: String, user_id: int) -> String:
		if not _can_run():
			return (password + str(user_id)).sha256_text()
		
		var salt := str(user_id).sha1_text()
		var command := COMMAND % [password, salt, ITERATIONS, MEMORY]
		
		var thread := Thread.new()
		thread.start(_thread.bind(command))
		
		return await thread.wait_to_finish()
	
	
	func _can_run() -> bool:
		if is_standalone and is_disabled:
			printerr("Argon2 may not be disabled outside the editor, as it " +
					"is a major security risk.")
			get_tree().quit(ERR_UNAUTHORIZED)
			return false
		elif is_disabled:
			printerr("Warning: Argon2 is very important. Install it properly when possible.")
			return false
		
		return true
	
	
	func _thread(command: String) -> String:
		var result: Array[String] = []
		
		var error := OS.execute(program, ["-c", command], result)
		
		if error:
			printerr("Argon2 failed with exit code: " + str(error))
			return "ERROR"
		
		return result[0].strip_edges()
	
	
	func _get_program() -> String:
		match os:
			"Windows":
				return "Cmd.exe"
			"Linux":
				return "bash"
			"OSX":
				return "I don't know"
			_:
				return "ERROR"
