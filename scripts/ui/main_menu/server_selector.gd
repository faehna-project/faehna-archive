extends Button


const CONFIG_PATH := "user://user_accounts"
@export var panel: Button
static var servers: Array[Server] = []


func _ready() -> void:
	pressed.connect(_on_pressed)


static func _static_init() -> void:
	if Network.is_server:
		return
	
	if not FileAccess.file_exists(CONFIG_PATH):
		return
	
	_load_servers()


func _on_pressed() -> void:
	pass


static func write_to_file() -> void:
	var file := FileAccess.open(CONFIG_PATH, FileAccess.WRITE)
	
	file.store_string(var_to_str(servers))
	
	file.close()


static func _load_servers() -> void:
	var file := FileAccess.open(CONFIG_PATH, FileAccess.READ)
	
	servers = str_to_var(file.get_as_text())
	
	file.close()


class Server:
	extends RefCounted
	
	var name: String
	var address: String
	var port: String
	var username: String
	var password: String
	
	var is_current := false
