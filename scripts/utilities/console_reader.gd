extends Node


var thread: Thread


func _ready() -> void:
	if not Network.is_server:
		set_physics_process(false)
		return
	
	thread = Thread.new()
	thread.start(_await_input)


func _physics_process(_delta: float) -> void:
	if not thread.is_alive():
		set_physics_process(false)
		
		await SaveManager.save()
		thread.wait_to_finish()
		
		get_tree().quit()


func _await_input() -> void:
	print("Enter command 'q' to quit.")
	
	while true:
		var exit := _read_command()
		
		if exit:
			break


func _read_command() -> bool:
	var command := OS.read_string_from_stdin().strip_edges()
	
	match command:
		"q":
			return true
		_:
			printerr("Command '" + command + "' not recognized.")
	
	return false
