class_name Character
extends CharacterBody3D


const Bio := preload("res://scripts/character/bio.gd")
const Movement := preload("res://scripts/character/movement.gd")

var id: int
var user_id: int

var bio := Bio.new()
var movement := Movement.new()
