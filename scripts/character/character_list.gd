extends RefCounted


const CHARACTER := preload("res://scenes/creatures/character.tscn")

var current: int = -1
var _list: Array[Character]


@rpc("any_peer", "reliable")
func request_insert(name: String) -> void:
	if current != -1:
		push_warning("Player attempted to insert character while in-game.")
		return
	
	var character := CHARACTER.instantiate()
	character.bio.name = name
	
	insert(character)


@rpc("any_peer", "reliable")
func request_remove(id: int) -> void:
	if current != -1:
		push_warning("Player attempted to remove character while in-game.")
		return
	
	remove(id)


func insert(character: Character) -> void:
	character.id = randi()
	
	_list.append(character)


func remove(id: int) -> void:
	for i in _list.size():
		var character := _list[i]
		
		if character.id == id:
			_list.remove_at(i)


func clear() -> void:
	for character in _list:
		character.free()


func serialize() -> String:
	return Marshalls.variant_to_base64(var_to_str(self))


static func deserialize(serialized: String) -> Player.CharacterList:
	return str_to_var(Marshalls.base64_to_variant(serialized))
