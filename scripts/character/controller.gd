extends Node


const Movement := preload("res://scripts/character/movement.gd")

const ACCELERATION: float = 4.0
const FRICTION: float = 4.0
const GRAVITY := Vector3(0, -9.8, 0)

const SENSITIVITY: float = 0.005
const CAPTURED := Input.MOUSE_MODE_CAPTURED
const FREE := Input.MOUSE_MODE_VISIBLE

var is_sprinting := false

@onready var character := get_parent() as CharacterBody3D
@onready var stats := character.movement as Movement
@onready var camera := $"../Camera"

var mouse_captured: bool:
	set(new):
		mouse_captured = new
		Input.mouse_mode = CAPTURED if mouse_captured else FREE


func _ready() -> void:
	if character.player != Players.client:
		set_process(false)
		set_process_input(false)
		return
	
	mouse_captured = true


func _process(_delta: float) -> void:
	if Input.is_action_just_pressed("character_toggle_mouselock"):
		mouse_captured = not mouse_captured
	
	if Input.is_action_pressed("character_sprint"):
		is_sprinting = true
	else:
		is_sprinting = false


func _physics_process(delta: float) -> void:
	if character.player != Players.client:
		return
	
	character.velocity += GRAVITY * delta
	
	if not character.is_on_floor():
		character.move_and_slide()
		return
	
	_check_jump()
	_check_movement(delta)
	
	character.move_and_slide()


func _input(event: InputEvent) -> void:
	if event is InputEventMouseMotion and Input.mouse_mode == CAPTURED:
		_rotate(event.relative)


func _check_jump() -> void:
	if Input.is_action_just_pressed("character_jump"):
		character.velocity.y += stats.jump


func _check_movement(delta) -> void:
	var speed: float = stats.sprint if is_sprinting else stats.run
	var direction := _get_move_direction() * speed
	
	var desired_velocity := Vector3(direction.x, character.velocity.y, direction.z)
	
	character.velocity = lerp(character.velocity, desired_velocity, ACCELERATION * delta)


func _get_move_direction() -> Vector3:
	var input := Input.get_vector("character_left", "character_right",
			"character_forward", "character_back")
	
	var direction := Vector3(input.x, 0, input.y).normalized()
	
	direction = direction.rotated(Vector3.UP, character.rotation.y)
	
	return direction


func _rotate(motion: Vector2) -> void:
	motion *= SENSITIVITY
	
	camera.rotation += Vector3(-motion.y, 0, 0)
	character.rotation += Vector3(0, -motion.x, 0)
	
	camera.rotation.x = clampf(camera.rotation.x, deg_to_rad(-89), deg_to_rad(89))
