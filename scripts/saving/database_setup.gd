extends Node

var database = Database.new(self)

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	database.query("""
		CREATE TABLE players (
			user_id SERIAL PRIMARY KEY,
			username VARCHAR(32) NOT NULL,
			password VARCHAR(64) NOT NULL,
			created_on BIGINT NOT NULL,
			last_online BIGINT NOT NULL,
			characters TEXT,
			notes TEXT
		);
	""")
