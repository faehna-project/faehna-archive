extends Node
class_name DatabaseConnection


signal data_received(data: Array[PostgreSQLClient])

const CONFIG_PATH := "user://server.cfg"
static var config := ConfigFile.new()

var database: PostgreSQLClient = PostgreSQLClient.new()
var db_name: String = config.get_value("Database", "database", "faehna")
var host: String = config.get_value("Database", "host", "127.0.0.1")
var port: int = config.get_value("Database", "port", "5432")
var user: String = config.get_value("Database", "user", "postgres")
var password: String = config.get_value("Database", "password", "NONE")


func _init(parent: Node) -> void:
	database.connection_established.connect(_connection_established)
	database.authentication_error.connect(_authentication_error)
	database.connection_closed.connect(_connection_closed)
	database.data_received.connect(_data_received)
	
	parent.add_child(self)
	
	database.connect_to_host("postgresql://%s:%s@%s:%d/%s" % [user, password, host, port, db_name])


static func _static_init() -> void:
	config.load(CONFIG_PATH)


func _physics_process(_delta: float) -> void:
	database.poll()


func _exit_tree() -> void:
	database.close()
	database = null


func execute(query: String) -> int:
	return database.execute(query)


func _connection_established() -> void:
	print_debug("Connection established with %s:%d/%s as %s." % [host, port, db_name, user])


func _authentication_error(error: Dictionary) -> void:
	printerr("Database Authentication Error:" + error.message)


func _connection_closed(was_clean_close: bool) -> void:
	if was_clean_close:
		print_debug("Connection with %s:%d/%s closed cleanly." % [host, port, db_name])
	else:
		printerr("Connection with %s:%d/%s did not close cleanly." % [host, port, db_name])


func _data_received(err: Dictionary, _ts: PostgreSQLClient.TransactionStatus, data: Array) -> void:
	#print("Data received from %s:%d/%s." % [host, port, db_name])
	if not err.is_empty():
		push_error("SQL Error: " + err.message)
		return
	
	data_received.emit(data)
