extends Node
class_name Database

func _init(parent: Node):
	name = parent.name + "Database"
	parent.add_child(self, false, Node.INTERNAL_MODE_BACK)

func get_values(table: String, values: String, condition: String) -> Array:
	var _query = "SELECT %s FROM %s WHERE %s;" % [values, table, condition]
	
	return await query_with_return(_query)

func insert_values(table: String, values: String) -> void:
	var _query = "INSERT INTO %s VALUES %s;" % [table, values]
	
	await query(_query)

func update_values(table: String, values: String, condition: String) -> void:
	var _query = "UPDATE %s SET %s WHERE %s;" % [table, values, condition]
	
	await query(_query)

func delete_value(table: String, condition: String) -> void:
	var _query = "DELETE FROM %s WHERE %s;" % [table, condition]
	
	await query(_query)

func query(_query: String) -> void:
	var connection := await get_connection()
	
	connection.execute(_query)
	
	await connection.data_received
	
	connection.queue_free()

func query_with_return(_query) -> Array:
	var connection := await get_connection()
	
	connection.execute(_query)
	
	var result = await connection.data_received
	
	connection.queue_free()
	
	# Get first command's return value.
	return result[0].data_row

func get_connection() -> DatabaseConnection:
	var connection = DatabaseConnection.new(self)
	
	await connection.database.connection_established
	
	return connection
