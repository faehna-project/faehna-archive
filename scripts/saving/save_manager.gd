extends Node
## Special class that manages data saving.


const time_between_saves: int = 300

const player_update_query := "last_online = %d, characters = '%s'"

var timer := Timer.new()
var database: Database


func _ready() -> void:
	if not Network.is_server:
		return
	
	database = Database.new(self)
	# Don't know why but this errors when you just put in database.free
	tree_exiting.connect(Callable(database, "free"))
	
	Players.child_exiting_tree.connect(save_player)
	
	add_child(timer)
	timer.start(time_between_saves)
	timer.timeout.connect(save)


func save() -> void:
	print("Saving data...")
	
	for player in Players.get_players():
		await save_player(player)
	
	print("Save complete.")


func save_player(player: Player):
	print("Saving player " + player.username + "...")
	
	var current_time := Time.get_unix_time_from_system()
	var characters := player.characters.serialize()
	
	var update_query := player_update_query % [current_time, characters]
	var condition = "user_id = " + str(player.user_id)
	
	await database.update_values("players", update_query, condition)
